import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
// import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/operator/map';


@Injectable({
  providedIn: 'root'
})
export class MailService {
  private headers = new HttpHeaders().set('Content-type','application/json')
  constructor( private http: HttpClient) { }
  send_mail(mail){
    // console.log(mail);
    return  this.http.post('https://mobilappy.herokuapp.com/send/',mail, {headers:this.headers});
  } 
}
