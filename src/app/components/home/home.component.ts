import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {MailService} from '../../mail.service';
import {Router, RouterModule} from '@angular/router';
import { NgxLoadingModule } from 'ngx-loading';




@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public loading = false;

  devices = ['Device Type','Mobile','Laptop/PC','Tablet'];
  cyties = ['SelectCity','Hyderabad','Visakhapatnam']; 
form = new FormGroup({
  Name : new FormControl('',Validators.required),
  Email : new FormControl('', [Validators.required, Validators.email]),
  Phone : new FormControl('',[Validators.required]),
  Device_Type : new FormControl('' ),
  Brand_and_Model : new FormControl('',Validators.required),
  Issue : new FormControl('', Validators.required),
  Best_time_for_device_pickup : new FormControl(''),
  City : new FormControl('')  
});

regexpEmail = new RegExp('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$');
regexpNumber: RegExp = /^[+ 0-9]{10}$/;
  constructor(private mail: MailService,private router:Router) { }

  ngOnInit() {
  }

  // convenience getter for easy access to form fields
  // get f() { return this.form.controls; }


  

  onSubmit(){

    // console.log(this.regexpEmail.test(this.form.value.Email));
    // console.log(this.regexpEmail.test(this.form.value.Email));
    // console.log('cool'+this.form.value.Device_Type);
    // console.log(JSON.stringify(this.form.value));
    if(this.form.value.Name =='' || this.form.value.Email == '' || this.form.value.Device_Type == '' || this.form.value.Phone ==  '' ||
      this.form.value.Brand_and_Model == '' || this.form.value.Issue == '' ||  this.form.value.Best_time_for_device_pickup   == '' || this.form.value.City == ''){
        alert('Place Fill All Fields');
        return;
        
      }
    else if(!(this.regexpEmail.test(this.form.value.Email))){
      alert('Email not valid');
      return;
    }
    else if(!(this.regexpNumber.test(this.form.value.Phone))){
      alert('Mobile number not valid');
      return
    }
    this.loading = true;
   this.mail.send_mail(this.form.value).subscribe(

      res =>{
        
        alert('Successfull');
        // window.location.reload();
        this.loading = false;
        this.router.navigateByUrl('/success');

        
      },
      err => {
        alert('Failed');
        this.loading = false;
        
      }
    );
    // if(this.form.invalid){

    //   alert('Pleace fill all details');
    //   alert(JSON.stringify(this.form.value));
    //   return;
    // }
    // salue));
  }

}
